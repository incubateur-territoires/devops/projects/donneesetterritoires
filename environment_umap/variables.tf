variable "gitlab_environment_scope" {
  type = string
}
variable "project_id" {
  type = number
}

variable "base_domain" {
  type = string
}

variable "monitoring_org_id" {
  type      = string
  sensitive = true
}

variable "sso_endpoint" {
  type = string
}
variable "sso_key" {
  type = string
}
variable "sso_secret" {
  type      = string
  sensitive = true
}

variable "backup_bucket_name" {
  type = string
}
variable "backup_bucket_endpoint" {
  type = string
}
variable "backup_bucket_region" {
  type = string
}
variable "backup_bucket_access_key" {
  type = string
}
variable "backup_bucket_secret_key" {
  type      = string
  sensitive = true
}
