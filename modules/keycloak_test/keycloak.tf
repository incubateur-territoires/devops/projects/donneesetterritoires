variable "hostname" {
  type    = string
  default = "keycloak.gristtest.donnees.dev.incubateur.anct.gouv.fr"
}
variable "monitoring_org_id" {
  type = string
}
resource "helm_release" "keycloak" {
  repository = "https://gitlab.com/api/v4/projects/47579096/packages/helm/stable"
  chart      = "webapp"
  name       = "keycloak"
  namespace  = var.namespace
  values = [
    <<-EOT
    image:
      repository: quay.io/keycloak/keycloak
      tag: "22.0"
    envVars:
      KC_HOSTNAME: ${var.hostname}
      KC_PROXY: edge
    strategy:
      type: Recreate
    args:
      - start
    service:
      targetPort: 8080
    probes:
      liveness:
        tcpSocket:
          port: 8080
      readiness:
        tcpSocket:
          port: 8080
    ingress:
      annotations:
        kubernetes.io/ingress.class: haproxy
      enabled: true
      hosts:
        - host: ${var.hostname}
          tls: true
    resources:
      limits:
        memory: 512Mi
      requests:
        cpu: 50m
    # force update ${local.force_update}
    EOT
    , <<-EOT
    podAnnotations:
      monitoring-org-id: ${var.monitoring_org_id}
    EOT
  ]
  postrender {
    binary_path = "${path.module}/kustomize.sh"
    args        = ["${path.module}/helm_manifest_kustomize.yaml"]
  }
}
locals {
  force_update = sha256(join("\n", [for filename in ["kustomize.sh", "kustomization.yaml", "multiple_fixes.yaml"] : file("${path.module}/${filename}")]))
}
resource "kubernetes_persistent_volume_claim_v1" "keycloak_data" {
  metadata {
    namespace = var.namespace
    name      = "keycloak-data"
  }
  spec {
    resources {
      requests = {
        storage = "1Gi"
      }
    }
    access_modes = ["ReadWriteOnce"]
  }
}

terraform {
  required_providers {
    helm = {
      source = "hashicorp/helm"
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
    }
  }
  required_version = ">= 0.14"
}
