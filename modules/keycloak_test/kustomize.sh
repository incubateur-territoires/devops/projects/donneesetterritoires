#!/bin/sh
cd "$(dirname "$1")" || exit 1
cat > "$(basename "$1")"
kubectl kustomize
rm "$(basename "$1")"
