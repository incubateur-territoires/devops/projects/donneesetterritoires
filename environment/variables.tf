variable "base_domain" {
  type = string
}

variable "gitlab_environment_scope" {
  type = string
}

variable "gitlab_project_ids" {
  type = object({
    zonage = number
    umap   = number
  })
}

variable "monitoring_org_id" {
  type = string
}
