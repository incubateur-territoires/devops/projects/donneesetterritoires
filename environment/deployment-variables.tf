module "configure_zonage_for_deployment" {
  source  = "gitlab.com/vigigloo/tf-modules/gitlabk8svariables"
  version = "0.1.0"

  repositories = [var.gitlab_project_ids.zonage]
  scope        = var.gitlab_environment_scope

  cluster_user           = var.kubeconfig.user
  cluster_token          = var.kubeconfig.token
  cluster_namespace      = var.namespace
  cluster_host           = var.kubeconfig.host
  cluster_ca_certificate = base64decode(var.kubeconfig.cluster_ca_certificate)

  base-domain = var.base_domain
}

resource "gitlab_project_variable" "db_host" {
  project           = var.gitlab_project_ids.zonage
  environment_scope = var.gitlab_environment_scope

  key   = "HELM_ENV_VAR_DBHOST"
  value = module.postgresql.host
}
resource "gitlab_project_variable" "db_port" {
  project           = var.gitlab_project_ids.zonage
  environment_scope = var.gitlab_environment_scope

  key   = "HELM_ENV_VAR_DBPORT"
  value = module.postgresql.port
}
resource "gitlab_project_variable" "db_name" {
  project           = var.gitlab_project_ids.zonage
  environment_scope = var.gitlab_environment_scope

  key   = "HELM_ENV_VAR_DBNAME"
  value = module.postgresql.dbname
}
resource "gitlab_project_variable" "db_user" {
  project           = var.gitlab_project_ids.zonage
  environment_scope = var.gitlab_environment_scope

  key   = "HELM_ENV_VAR_DBUSER"
  value = module.postgresql.user
}
resource "gitlab_project_variable" "db_pass" {
  project           = var.gitlab_project_ids.zonage
  environment_scope = var.gitlab_environment_scope

  key   = "HELM_ENV_VAR_DBPASS"
  value = module.postgresql.password
}

resource "gitlab_project_variable" "zonage_helm_values" {
  project           = var.gitlab_project_ids.zonage
  environment_scope = var.gitlab_environment_scope
  variable_type     = "file"
  protected         = false

  key   = "HELM_UPGRADE_VALUES"
  value = <<-EOT
  resources:
    limits:
      memory: 1024Mi
    requests:
      cpu: 10m

  ingress:
    enabled: true
    annotations:
      kubernetes.io/ingress.class: haproxy

  podAnnotations:
    monitoring-org-id: ${var.monitoring_org_id}

  service:
    targetPort: 80

  probes:
    liveness:
      tcpSocket:
        port: 80
    readiness:
      tcpSocket:
        port: 80

  monitoring:
    exporter:
      enabled: true
    dashboard:
      deploy: true

  EOT
}
