variable "base_domain" {
  type = string
}

variable "project_id" {
  type = string
}

variable "gitlab_environment_scope" {
  type = string
}

variable "monitoring_org_id" {
  type = string
}
