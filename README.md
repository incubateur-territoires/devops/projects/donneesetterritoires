#

## Setting up

Instructions for setting up OpenTofu can be found in [the main infrastructure repository](https://gitlab.com/incubateur-territoires/devops/infrastructure/).

## Migrating from sqlite home DB to PostgreSQL

Here's the high level process:
- Restart the grist container with a sleep command, no probe and DB config in its environment variables.
- Run a shell on the container and run `./sandbox/run.sh` to create the model in the DB.
- `^C` once the DB setup is completed.
- Configure and run the following script:
```bash
export PG_CONNECTION_URI=todo
export PGPASSWORD=todo

apt update
apt install -y git sbcl unzip libsqlite3-dev make curl gawk freetds-dev libzip-dev
git clone https://github.com/dimitri/pgloader.git
cd pgloader
make pgloader
./build/bin/pgloader --help

./build/bin/pgloader --with "truncate" --with "data only" \
  --no-ssl-cert-verification \
  sqlite:///persist/home.sqlite3 \
  "$PG_CONNECTION_URI?sslmode=allow"
```

Afterwards, restart grist with its normal config (command and probes).
