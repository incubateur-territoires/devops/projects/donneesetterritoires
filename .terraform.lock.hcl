# This file is maintained automatically by "tofu init".
# Manual edits may be lost in future updates.

provider "registry.opentofu.org/gitlabhq/gitlab" {
  version     = "3.12.0"
  constraints = "~> 3.12.0"
  hashes = [
    "h1:zZKpUsf1STvLLyH30a6ZcjQoARU8MJGbHsGCjYHrnOA=",
    "zh:1c3e89cf19118fc07d7b04257251fc9897e722c16e0a0df7b07fcd261f8c12e7",
    "zh:207f7ffaaf16f3d0db9eec847867871f4a9eb9bb9109d9ab9c9baffade1a4688",
    "zh:2360bdd3253a15fbbb6581759aa9499235de670d13f02272cbe408cb425dc625",
    "zh:2a9bec466163baeb70474fab7b43aababdf28b240f2f4917299dfe1a952fd983",
    "zh:3a6ea735b5324c17aa6776bff3b2c1c2aeb2b6c20b9c944075c8244367eb7e4c",
    "zh:3aa73c622187c06c417e1cd98cd83f9a2982c478e4f9f6cd76fbfaec3f6b36e8",
    "zh:51ace107c8ba3f2bb7f7d246db6a0428ef94aafceca38df5908167f336963ec8",
    "zh:53a35a827596178c2a12cf33d1f0d0b4cf38cd32d2cdfe2443bdd6ebb06561be",
    "zh:5bece68a724bffd2e66293429319601d81ac3186bf93337c797511672f80efd0",
    "zh:60f21e8737be59933a923c085dcb7674fcd44d8a5f2e4738edc70ae726666204",
    "zh:9fb277f13dd8f81dee7f93230e7d1593aca49c4788360c3f2d4e42b8f6bb1a8f",
    "zh:ac63a9b4a3a50a3164ee26f1e64cc6878fcdb414f50106abe3dbeb7532edc8cd",
    "zh:ed083de9fce2753e3dfeaa1356508ecb0f218fbace8a8ef7b56f3f324fff593b",
    "zh:ed966026c76a334b6bd6d621c06c5db4a6409468e4dd99d9ad099d3a9de22768",
  ]
}

provider "registry.opentofu.org/grafana/grafana" {
  version     = "1.27.0"
  constraints = "~> 1.27.0"
  hashes = [
    "h1:ifKgRrZHspeXMSavSCsWbLrk6tC1NOyWN0iiGv2j6I4=",
    "zh:01ef0ae20530a54cbb4bffc35e97733916e5ae2e8f7fa00aefa2e86e24206823",
    "zh:08a4ac8b690bab9a3b454c3d998917f4ed49fc225a21ff53ceb0488eb4b9d15d",
    "zh:0e08516cd6c2495bc83a4a8e0252bfa70e310aa400af0fe766bbe7ddd05a21cb",
    "zh:14856865f6e6695e6d7708d70844a2c031cfc9b091e7cf530a453b2f78c9a691",
    "zh:2b1c05fff5011ab83acdd292484857fe886cd113abbb7fc617bbb8f358517cc0",
    "zh:31bae1b1c635a94329470b30986d336f4b3819bf24aacd953d5b57debb83bd4d",
    "zh:352b6ea190711c8f3f107540c8943c8f6b9faf4fbc73a9c1721b15db4a103edb",
    "zh:7eda29d30d451b842c5b0b2cf15cb907e76e8bac4843e90830a62a68bbe877a5",
    "zh:bd640d7e8a126d810a34766816b4e17a07c634ffef14b468269c8191683fff27",
    "zh:ddfa43a7b31fb840f04420c82fe0313a44fa5099c3d1f61219e630d6c8440e2d",
    "zh:e50dccaf8cb9922ac25e2f87a85083d5c2cef5323eac4ce7d933012af7a25e88",
    "zh:e72903aeb4830b7b89efcf7336a61c736d9049c4156b6f17cec51663ed6e803d",
    "zh:f4161d62960ec9f9d84cb73437a9b9195831c467cdcc3381e431fa6e2cd92a14",
    "zh:f4699da872dfc9847eb3da49fd6ae4943e92602b617931bb07b91e646d90a279",
  ]
}

provider "registry.opentofu.org/hashicorp/helm" {
  version     = "2.15.0"
  constraints = "~> 2.0"
  hashes = [
    "h1:Cak4To+L1FxO1qkb2Gaonyjtdpu+vglc6NHynyUHylw=",
    "zh:21394ae3ec6f8ccda74688f8aeb979c03c9c52b60b5d0ada10521b5a75ae85af",
    "zh:248ba25e309432dc7a2a6049da9178731ae3884be1761c4e349c844ce5159d82",
    "zh:30dd6046b239f8b3788958475ad4db9b956c99ea71a0492fe6f2380d8d711ffc",
    "zh:40691066592cdd396226ff0ecd4153dce91799375282c3c8a13fdf21d616c73b",
    "zh:54b16f5ac335903f6bd6c7ba03c66b894940511a0d16c6ad92a16fe9ef80aaa8",
    "zh:9af1702deec999a8ba5fa379de6eb515bf8b045bb02a7f24e3aa1a559f88ec12",
    "zh:d057a371798b526b32d6985baaaf6e8126f14f23e1ebd65b44b970064c7790e5",
    "zh:de6fa77b4763ccdcf8d5546d54609299e3b0a2cfe3446e62d5cfa7806e2aa003",
    "zh:e2a21a57031a97abd3a61c09ffa84f4aae451329e876c2cd6597e02947ca1008",
    "zh:f8d12702874a935e0e2397bdb050f4d4c0d83fb4c0a9c7dfd1b49257605149bd",
  ]
}

provider "registry.opentofu.org/hashicorp/kubernetes" {
  version     = "2.13.1"
  constraints = "~> 2.0, >= 2.13.0, ~> 2.13.0, ~> 2.13"
  hashes = [
    "h1:fY2gWOd+w5CKV+B+T9LugeprJxHjjkaW81QZ4Urpzlk=",
    "zh:15d61e1ea6e428437e5947330a8328ed62b61bca01e0f2584e4e80d0aaea93ab",
    "zh:61183fddab8988a892f459abb38d9d6e50b8db0e2b252e398a10270e9a4f3328",
    "zh:67db11bf1596595ada2ae4e8fce13ac3e7ffd242ff8f6a2440bd8086c0d2cd09",
    "zh:6be96982825d821df7b29dc0c40c2f5807561e6af33e858668d1e06e1df3efc3",
    "zh:6e391b9601349ded72135e5f9159161f3cf9042e272611c2b5d863bda2724302",
    "zh:73dd61f4dc6f03f58c643a5dcee8170520f38ec06248186b651ea3d951ed7d6a",
    "zh:92b2f3f0657cfe0ad7cfba72e89246f5d02be53389a56fc205716b549e4848cd",
    "zh:a7ff7812eebdcaf75bb8937acc742d8f7d796d2df93d2c2feb27c5813e548da1",
    "zh:ac2edd95b356ac0fb48569c825cb91dfc0820c688ef12cc657c2305b8f12be94",
    "zh:eabae340b652d63f8f127771e70af854671bf76014f8c53e1f95b09d42729e0d",
  ]
}

provider "registry.opentofu.org/hashicorp/random" {
  version     = "3.5.1"
  constraints = "~> 3.5.1"
  hashes = [
    "h1:tW+G7lgqbHUtraKHPWuotYHlME1vcAf50YvOeHQlGHg=",
    "zh:0002dd4c79453da5bf1bb9c52172a25d042a571f6df131b7c9ced3d1f8f3eb44",
    "zh:49b0f8c2bd5632799aa6113e0e46acaa7d008f927665a41a1f8e8559fe6d8165",
    "zh:56df70fca236caa06d0e636c41ab71dd1ced05375f4ddcb905b0ed2105737048",
    "zh:58e4de40540c86b9e2e2595dac1318ba057718961a467fa9727866f747693eb2",
    "zh:5992f11c738812ccd7476d4c607cb8b76dea5aa612be491150c89957ec395ddd",
    "zh:7ff4f0b7707b51737f684e96d85a47f0dd8be0f72a3c27b0798755d3faad15e2",
    "zh:8e4b0972e216c9773ab525accfa36eb27c44c751b06b125ecc53f4226c91cea8",
    "zh:d8956cc5abcd5d1173b6cc25d5d8ed2c5cc456edab2fddb774a17d45e84820cb",
    "zh:df7f9eb93a832e66bc20cc41c57d38954f87671ec60be09fa866273adb8d9353",
    "zh:eb583d8f03b11f0b6c535375d8ed0d29e5f7f537b5c78943856d2e8ce76482d9",
  ]
}

provider "registry.opentofu.org/hashicorp/time" {
  version     = "0.12.0"
  constraints = ">= 0.7.0"
  hashes = [
    "h1:Om7xF0GgRkBsAjKis3RAFXQJKmHgnO04C+PEScF/xTM=",
    "zh:01b7ac8203eb7ed712a356215e44f8851b96ddcfdf63b13ff9f870f799667059",
    "zh:06c4420bdb964209eb119f1740575df7b8ac44a3b5d71631dae2962a155f58b7",
    "zh:2534d1d04ca934e25426ab5bb0b29a57a95c676f70b154bfb382d58bf1e6f6c9",
    "zh:340de6c71a1090f13ab5c429ca2134c12189e8b86c2b104859e82eb30eea9772",
    "zh:561a2780f7fb1b0a9092c59c4eb3e3d8c3ec9cecddc9214ae92fdc941c3bd2e7",
    "zh:65b1a982617375123bc3a1dcd44d61264cabac6b3d83378e7079ee0655ec6679",
    "zh:9ae9f6c9609c5ed9e35a702068629ef5adfb131f957a571fc39ce0127c782ca4",
    "zh:ad7f066c5db340683cb5a3a29ced3a2ece13c5b84c46d6b3d30815444a6c78ee",
    "zh:f532d2c33c2303a970e9ee813e37d208eb65321aec489da14786b7f04ea66105",
    "zh:fb269e2425a4b996fef79665eaeec8f40a388bf7ac7bf8ce2c108fb83c4b10ca",
  ]
}

provider "registry.opentofu.org/scaleway/scaleway" {
  version     = "2.43.0"
  constraints = "~> 2.2"
  hashes = [
    "h1:adTVxpdKkSUVDasMWHrNqoSRDD6ztSVXONOEhnmIkyY=",
    "zh:019b1d05013bed2ac7687d64ff51a5b150cefacaef4cc752d677cdb0c06b07a0",
    "zh:0fc7a5e0178774945ca8135585ea51d755da66a2083e88e87b522efa058ac556",
    "zh:295ee6f2b45deb01b0961d189110ad704b3634026d7d3ace424dba7a51623cf1",
    "zh:526ef9b9a5678ab61537ba021f2421b8d11d893e3fdfcef15c720d309631ede0",
    "zh:6624284c6424fe07c9cfd09204174c44716d26ed8b48d2d13ceaa937c3eb0b8a",
    "zh:6ccc51561986facc7f8b25e148fa6b528afb04b65a8df7afba73fe65cf6c2f04",
    "zh:790b74d9d85c8596fe5974cfb59740508668fb65c6dea04f08f769c08c917446",
    "zh:9b58cd255511124458b03dac23e2db2625c8f2ef3148ea3ca10a514511233416",
    "zh:b339ad67e9a7bbe02382d1c48b633e1a3da0c3c245093a50a86fedf33548339c",
    "zh:cee426f008289568f20297775d689734fd674a03c2c9b9691fb38f94c4c0ab34",
    "zh:e3f0e06190767aff4a2d7242a865f7ac2963eb59a00d86b2a8359911e2d514d1",
    "zh:ef958ad54d4e6cd4d76a5fbe86a051fb411998a27cb1cf7229c05463d0ad535e",
    "zh:f8608db4e7e7156c4f7f7205e8b3a2095a49e115402257323d0180a8bafc2d2e",
    "zh:f88ce1874d5f2faa06c81bc1666ecf8ec1cc3dd7ffe68688c17ae8a2f30692e4",
  ]
}
