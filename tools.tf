locals {
  dns_zone_incubateur = "incubateur.anct.gouv.fr"
  prod_email          = "donnees@anct.gouv.fr"
  beta_email          = "contact@grist.numerique.gouv.fr"
  grist_common_env = {
    GRIST_WIDGET_LIST_URL              = "https://betagouv.github.io/grist-custom-widgets-fr-admin/widget-list.json"
    ALLOWED_WEBHOOK_DOMAINS            = "gouv.fr"
    GRIST_MAX_UPLOAD_ATTACHMENT_MB     = "10"
    GRIST_MAX_UPLOAD_IMPORT_MB         = "100"
    GRIST_TEMPLATE_ORG                 = "templates"
    PERMITTED_CUSTOM_WIDGETS           = "calendar"
    GRIST_HELP_CENTER                  = "https://support.getgrist.com/fr"
    GRIST_DEFAULT_LOCALE               = "fr"
    GRIST_HIDE_UI_ELEMENTS             = "billing,sendToDrive,supportGrist"
    GRIST_DOCS_MINIO_USE_SSL           = 1
    COMMENTS                           = true
    ASSISTANT_MODEL                    = "meta-llama/Meta-Llama-3.1-70B-Instruct"
    ASSISTANT_CHAT_COMPLETION_ENDPOINT = "https://albert.api.etalab.gouv.fr/v1/chat/completions"
    ASSISTANT_API_KEY                  = var.tools_grist_common_assistant_api_key
    NODE_OPTIONS                       = "--max-old-space-size=4096"
    GRIST_ACTION_HISTORY_MAX_BYTES     = 10 * 1024 * 1024 # 10 Mo
    PYTHON_VERSION                     = 3
  }
  grist_prod_env = {
    FREE_COACHING_CALL_URL     = "mailto:${local.prod_email}"
    GRIST_CONTACT_SUPPORT_URL  = "mailto:${local.prod_email}"
    GRIST_TERMS_OF_SERVICE_URL = "https://pad.numerique.gouv.fr/s/YHUvoTK0L#"
    ALLOWED_WEBHOOK_DOMAINS    = "gouv.fr,n8n.oriatec.fr"
  }
  grist_beta_env = {
    FREE_COACHING_CALL_URL     = "mailto:${local.beta_email}"
    GRIST_CONTACT_SUPPORT_URL  = "mailto:${local.beta_email}"
    GRIST_TERMS_OF_SERVICE_URL = "https://pad.numerique.gouv.fr/s/CESJtoIZ1#"
  }
  cors_allow_origin          = "^https://.*\\\\.anct\\\\.gouv\\\\.fr$"
  lasuite_grist_oci_registry = "lasuite/grist"
  lasuite_grist_oci_tag      = "1.2.1-1"
  grist_oci_registry         = "gristlabs/grist"
  grist_oci_tag              = "1.2.1"
}

module "addok" {
  source     = "./tools/addok"
  kubeconfig = var.kubernetes_config.dev.donneesetterritoires-addok
  namespace  = "donneesetterritoires-addok"

  hostname = "addok.${var.dev_base_domain}"
}

module "grist_beta" {
  source     = "./tools/grist_ha"
  kubeconfig = var.kubernetes_config.prod.donnees-grist-beta
  namespace  = "donnees-grist-beta"

  default_email           = var.tools_grist_beta_default_email
  monitoring_org_id       = random_string.production_secret_org_id.result
  oauth_client_id         = var.tools_grist_beta_oauth_client_id
  oauth_client_secret     = var.tools_grist_beta_oauth_client_secret
  oauth_domain            = var.tools_grist_beta_oauth_domain
  domain                  = "grist.numerique.gouv.fr"
  project_slug            = "${var.project_slug}-grist-beta"
  scaleway_project_config = var.scaleway_project_config
  cors_allow_origin       = local.cors_allow_origin
  image_repository        = local.lasuite_grist_oci_registry
  image_tag               = local.lasuite_grist_oci_tag
  database_volume_size    = "15Gi"
  grist_extra_env         = merge(local.grist_common_env, local.grist_beta_env)
  with_security_txt       = 0

  oauth_scopes               = "openid email"
  oidc_protections           = "STATE,NONCE"
  oidc_acr_values            = "eidas1"
  oidc_extra_metadata        = "{\"id_token_signed_response_alg\": \"RS256\", \"userinfo_signed_response_alg\": \"RS256\"}"
  oidc_ignore_email_verified = "true"

  grist_doc_wk_limits_memory_mb  = 6 * 1024
  grist_doc_wk_requests_cpu_m    = 500
  grist_home_wk_limits_memory_mb = 0.5 * 1024
  grist_home_wk_requests_cpu_m   = 200
  grist_doc_wk_replicas          = 12
  grist_home_wk_replicas         = 2
  gvisor_limit_memory_bytes      = 1 * 1024 * 1024 * 1024

  providers = {
    scaleway = scaleway.project
  }
}

module "grist_prod" {
  source     = "./tools/grist_ha"
  kubeconfig = var.kubernetes_config.prod.grist
  namespace  = "grist"

  scaleway_project_config = var.scaleway_project_config
  domain                  = "grist.${local.dns_zone_incubateur}"
  oauth_client_id         = var.production_tools_grist_oauth_client_id
  oauth_client_secret     = var.production_tools_grist_oauth_client_secret
  oauth_domain            = var.production_tools_grist_oauth_domain
  default_email           = var.production_tools_grist_default_email
  project_slug            = "${var.project_slug}-grist-production"
  override_namespace      = "grist"
  grist_extra_env         = merge(local.grist_common_env, local.grist_prod_env)
  cors_allow_origin       = local.cors_allow_origin
  image_repository        = local.grist_oci_registry
  image_tag               = local.grist_oci_tag
  database_volume_size    = "10Gi"
  custom_script = {
    path    = "/grist/static/custom.js"
    content = filebase64("${path.module}/custom_prod.js")
  }
  custom_css = {
    path    = "/grist/static/custom.css",
    content = filebase64("${path.module}/tools/grist_ha/custom-anct.css")
  }
  custom_logo = {
    path    = "/grist/static/ui-icons/Logo/logo_anct_2022.svg",
    content = filebase64("${path.module}/tools/grist_ha/logo_anct_2022.svg")
  }

  grist_doc_wk_limits_memory_mb  = 6 * 1024
  grist_doc_wk_requests_cpu_m    = 500
  grist_home_wk_limits_memory_mb = 0.5 * 1024
  grist_home_wk_requests_cpu_m   = 200
  grist_doc_wk_replicas          = 10
  grist_home_wk_replicas         = 3
  gvisor_limit_memory_bytes      = 1 * 1024 * 1024 * 1024

  monitoring_org_id = random_string.production_secret_org_id.result
  providers = {
    scaleway = scaleway.project
  }
}
