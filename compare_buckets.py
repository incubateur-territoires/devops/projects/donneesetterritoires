import json
import re

RE_DOCID = '(?P<docid>[a-zA-Z0-9~]+)'
WITH_LIST = True


def read_bucket_list(filename):
    result = {
      'meta': {},
      'docs': {},
    }
    with open(filename) as f:
        lines = f.readlines()
        for line in lines:
            line_data = json.loads(line)
            key = line_data['key']
            match_doc = re.match(f'docs/{RE_DOCID}.grist', key)
            match_meta = re.match(f'docs/assets/unversioned/{RE_DOCID}/meta.json', key)
            if match_doc:
                docid = match_doc.group('docid')
                if docid.startswith('new~'):
                    continue
                result['docs'][docid] = {'key': key, 'size': line_data['size']}
            elif match_meta:
                docid = match_meta.group('docid')
                if docid.startswith('new~'):
                    continue
                result['meta'][docid] = {'key': key, 'size': line_data['size']}
            else:
                print("Unrecognized file")
                print(key)
    return result


def print_set_diff(msg, a, b):
    diff = a.difference(b)
    print(f"{msg} ({len(diff)})")
    if WITH_LIST:
        for id in diff:
            print(id)
        print('*' * 80)


outscale_data = read_bucket_list('bucket-outscale.json')
scaleway_data = read_bucket_list('bucket-scaleway.json')

outscale_docs = set(outscale_data['docs'].keys())
outscale_meta = set(outscale_data['meta'].keys())

scaleway_docs = set(scaleway_data['docs'].keys())
scaleway_meta = set(scaleway_data['meta'].keys())

print_set_diff('Docs in outscale without meta.json', outscale_docs, outscale_meta)

print_set_diff('meta.json in outscale without doc', outscale_meta, outscale_docs)

print_set_diff('Docs in scaleway without meta.json', scaleway_docs, scaleway_meta)

print_set_diff('meta.json in scaleway without doc', scaleway_meta, scaleway_docs)

print_set_diff('Docs in scaleway not in outscale', scaleway_docs, outscale_docs)

print_set_diff('Docs in outscale not in scaleway', outscale_docs, scaleway_docs)

print_set_diff('meta.json in scaleway not in outscale', scaleway_meta, outscale_meta)

print_set_diff('meta.json in outscale not in scaleway', outscale_meta, scaleway_meta)

docs_to_copy = scaleway_docs.difference(outscale_docs)
total_size = 0
for docid in docs_to_copy:
    size = scaleway_data['docs'][docid]['size']
    total_size += size
print(f'Total size of scaleway docs not in outscale ({total_size / 1024 / 1024 / 1024} Gi)')

empty_docs_to_copy = 0
for docid in docs_to_copy:
    size = scaleway_data['docs'][docid]['size']
    if size == 0:
        empty_docs_to_copy += 1
print(f'Docs to copy with size 0 ({empty_docs_to_copy})')
