locals {
  deploy_redirect = var.redirect_from != null
}
resource "kubernetes_ingress_v1" "redirect" {
  count = local.deploy_redirect ? 1 : 0
  metadata {
    name      = "redirect"
    namespace = var.namespace
    annotations = {
      "cert-manager.io/cluster-issuer" = "letsencrypt-prod"
    }
  }

  spec {
    ingress_class_name = "haproxy"
    rule {
      host = var.redirect_from
      http {
        path {
          path      = "/"
          path_type = "Prefix"
          backend {
            service {
              name = "redirect"
              port {
                number = 80
              }
            }
          }
        }
      }
    }
    tls {
      hosts       = [var.redirect_from]
      secret_name = "grist-old-tls"
    }
  }
}

resource "kubernetes_service_v1" "redirect" {
  count = local.deploy_redirect ? 1 : 0
  metadata {
    name      = "redirect"
    namespace = var.namespace
  }
  spec {
    selector = {
      "app.kubernetes.io/name" = "redirect"
    }
    port {
      port        = 80
      target_port = 80
    }
  }
}

resource "kubernetes_deployment_v1" "redirect" {
  count = local.deploy_redirect ? 1 : 0
  metadata {
    name      = "redirect"
    namespace = var.namespace
  }
  spec {
    selector {
      match_labels = {
        "app.kubernetes.io/name" = "redirect"
      }

    }
    template {
      metadata {
        name      = "redirect"
        namespace = var.namespace
        labels = {
          "app.kubernetes.io/name" = "redirect"
        }
      }
      spec {
        container {
          name  = "nginx"
          image = "docker.io/nginx:1.27"
          port {
            container_port = 80
          }
          volume_mount {
            name       = "redirect"
            mount_path = "/etc/nginx/conf.d"
          }
        }
        volume {
          name = "redirect"
          config_map {
            name = "redirect"
          }
        }
      }
    }
  }
}

resource "kubernetes_config_map_v1" "redirect" {
  count = local.deploy_redirect ? 1 : 0
  metadata {
    name      = "redirect"
    namespace = var.namespace
  }
  data = {
    "default.conf" = <<-EOT
    server {
      listen       80;
      server_name  localhost;
      return 308 https://${var.domain}$uri;
    }
    EOT
  }
}
