locals {
  grist_release_name = "grist"
  grist_namespace    = var.override_namespace != null ? var.override_namespace : var.project_slug
  grist_typeorm_config = indent(2, <<-EOT
      TYPEORM_TYPE: postgres
      TYPEORM_DATABASE:
        secretKeyRef:
          key: dbname
          name: "${module.postgresql.secret-name}"
      TYPEORM_HOST:
        secretKeyRef:
          key: host
          name: "${module.postgresql.secret-name}"
      TYPEORM_USERNAME:
        secretKeyRef:
          key: user
          name: "${module.postgresql.secret-name}"
      TYPEORM_PASSWORD:
        secretKeyRef:
          key: password
          name: "${module.postgresql.secret-name}"
      TYPEORM_EXTRA: '{"ssl": true, "extra": {"ssl": {"rejectUnauthorized": false}}}'
      EOT
  )
  grist_custom_script_env = var.custom_script == null ? "" : yamlencode({
    GRIST_INCLUDE_CUSTOM_SCRIPT_URL = format("%s?_cacheDate=%s", element(split("/", var.custom_script.path), length(split("/", var.custom_script.path)) - 1), "20241125")
  })
  grist_custom_css_yaml = var.custom_css == null ? "" : yamlencode(
    [
      {
        path    = var.custom_css.path,
        content = var.custom_css.content,
      },
      {
        path    = var.custom_logo.path,
        content = var.custom_logo.content,
      }
    ]
  )
  grist_custom_script_yaml = var.custom_script == null ? "" : yamlencode(
    [
      {
        path    = var.custom_script.path,
        content = var.custom_script.content,
      },
    ]
  )
}

resource "scaleway_object_bucket" "grist_snapshots" {
  name = "${var.project_slug}-snapshots"
  versioning {
    enabled = true
  }
}

resource "random_password" "redis_password" {
  length  = 32
  special = false
}

module "redis" {
  source        = "gitlab.com/vigigloo/tools-k8s/redis"
  version       = "0.1.1"
  namespace     = var.namespace
  chart_name    = "redis"
  chart_version = "17.4.2"

  redis_password = resource.random_password.redis_password.result
  redis_replicas = 0
}

resource "random_password" "grist_boot_key" {
  length  = 128
  special = false
}

locals {
  grist_oidc_config = merge({
    GRIST_OIDC_IDP_ISSUER = "https://${var.oauth_domain}/.well-known/openid-configuration"
    GRIST_OIDC_IDP_CLIENT_ID = {
      secretKeyRef = {
        name = kubernetes_secret_v1.grist_secret.metadata[0].name
        key  = "oauth_client_id"
      }
    }
    GRIST_OIDC_IDP_CLIENT_SECRET = {
      secretKeyRef = {
        name = kubernetes_secret_v1.grist_secret.metadata[0].name
        key  = "oauth_client_secret"
      }
    }
    GRIST_OIDC_IDP_SCOPES = var.oauth_scopes
    },
    # using compact([null]) to ensure null variables stay unset
    { for v in compact([var.oidc_protections]) : "GRIST_OIDC_IDP_ENABLED_PROTECTIONS" => v },
    { for v in compact([var.oidc_acr_values]) : "GRIST_OIDC_IDP_ACR_VALUES" => v },
    { for v in compact([var.oidc_extra_metadata]) : "GRIST_OIDC_IDP_EXTRA_CLIENT_METADATA" => v },
    { for v in compact([var.oidc_ignore_email_verified]) : "GRIST_OIDC_SP_IGNORE_EMAIL_VERIFIED" => v },
  )
}

resource "helm_release" "grist" {
  repository = "https://numerique-gouv.github.io/helm-charts/"
  chart      = "grist"
  version    = "5.3.9"
  name       = local.grist_release_name
  namespace  = local.grist_namespace
  timeout    = 600
  values = [
    <<-EOT
    image:
      repository: ${var.image_repository}
      tag: ${var.image_tag}

    commonEnvVars: &commonEnvVars
      APP_HOME_URL: https://${var.domain}
      APP_HOME_INTERNAL_URL: http://${local.grist_release_name}-home-wk
      GRIST_ORG_IN_PATH: "true"
      GRIST_DEFAULT_EMAIL: "${var.default_email}"
      GRIST_SANDBOX_FLAVOR: gvisor
      APP_STATIC_INCLUDE_CUSTOM_CSS: true
      ${local.grist_custom_script_env}
      GRIST_ANON_PLAYGROUND: false
      GRIST_PROMCLIENT_PORT: "9102"
      GRIST_MANAGED_WORKERS: true
      GRIST_SKIP_REDIS_CHECKSUM_MISMATCH: true
      ${length(var.grist_extra_env) > 0 ? indent(2, yamlencode(var.grist_extra_env)) : ""}

      GRIST_ALLOWED_HOSTS: ${var.domain}
      GRIST_LOG_SKIP_HTTP: ""
      GRIST_SINGLE_PORT: 0

      ${local.grist_typeorm_config}
      REDIS_URL:
        secretKeyRef:
          name: ${kubernetes_secret_v1.grist_secret.metadata[0].name}
          key: redis_url

      ${indent(2, yamlencode(local.grist_oidc_config))}

    commonPodAnnotations: &commonPodAnnotations
      monitoring-org-id: ${var.monitoring_org_id}
      prometheus.io/scrape: "true"
      prometheus.io/path: "/"
      prometheus.io/port: "9102"

    mountFiles:
      ${indent(2, local.grist_custom_script_yaml)}
      ${indent(2, local.grist_custom_css_yaml)}

    docWorker:
      replicas: ${var.grist_doc_wk_replicas}
      envVars:
        <<: *commonEnvVars

        GRIST_SERVERS: docs

        GRIST_DOCS_MINIO_BUCKET: ${scaleway_object_bucket.grist_snapshots.name}
        GRIST_DOCS_MINIO_ENDPOINT: ${var.s3_endpoint}
        GRIST_DOCS_MINIO_BUCKET_REGION: fr-par
        GRIST_DOCS_MINIO_ACCESS_KEY:
          secretKeyRef:
            name: ${kubernetes_secret_v1.grist_secret.metadata[0].name}
            key: s3_access_key
        GRIST_DOCS_MINIO_SECRET_KEY:
          secretKeyRef:
            name: ${kubernetes_secret_v1.grist_secret.metadata[0].name}
            key: s3_secret_key

        GVISOR_LIMIT_MEMORY: "${var.gvisor_limit_memory_bytes}"

      podAnnotations:
        <<: *commonPodAnnotations

      resources:
        limits:
          memory: ${local.grist_doc_wk_container_limits_memory}
        requests:
          cpu: ${local.grist_doc_wk_container_requests_cpu}

      persistence:
        core-dump:
          type: emptyDir
          mountPath: /tmp/core_dumps

      shareProcessNamespace: true

    homeWorker:
      replicas: ${var.grist_home_wk_replicas}
      envVars:
        <<: *commonEnvVars

        GRIST_SERVERS: home,static

      podAnnotations:
        <<: *commonPodAnnotations

      resources:
        limits:
          memory: ${local.grist_home_wk_container_limits_memory}
        requests:
          cpu: ${local.grist_home_wk_container_requests_cpu}

      persistence:
        core-dump:
          type: emptyDir
          mountPath: /tmp/core_dumps

      shareProcessNamespace: true

    loadBalancer:
      replicas: 2

    ingress:
      enabled: true
      host: ${var.domain}
      annotations:
        acme.cert-manager.io/http01-edit-in-place: "true"
        cert-manager.io/cluster-issuer: letsencrypt-prod
        kubernetes.io/ingress.class: haproxy
        haproxy.org/cors-enable: "true"
        haproxy.org/cors-allow-origin: "${var.cors_allow_origin}"
        haproxy.org/cors-allow-methods: "GET"
        haproxy.org/cors-allow-headers: "*"
        haproxy.org/response-set-header: X-Robots-Tag none
    EOT
  ]
}

moved {
  from = module.grist_namespace
  to   = module.namespace
}
moved {
  from = module.grist.helm_release.grist
  to   = helm_release.grist
}
