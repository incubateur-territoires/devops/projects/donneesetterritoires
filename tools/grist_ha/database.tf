resource "scaleway_object_bucket" "db_backups" {
  name = "${var.project_slug}-db-backups"
}

module "postgresql" {
  source  = "gitlab.com/vigigloo/tools-k8s-crunchydata/pgcluster"
  version = "0.0.22"

  chart_name = "postgresql"
  namespace  = var.namespace

  pg_volume_size = var.database_volume_size
  pg_replicas    = 2

  values = [
    <<-EOT
    multiBackupRepos:
    - volume:
        backupsSize: 40Gi
      schedules:
        full: 45 2 * * 0
        incremental: 45 2 * * 1-6
      retentionFull:
        type: count
        value: "4"
    - s3:
        bucket: ${scaleway_object_bucket.db_backups.name}
        endpoint: ${var.s3_endpoint}
        key: ${var.scaleway_project_config.access_key}
        keySecret: ${var.scaleway_project_config.secret_key}
        region: fr-par
      schedules:
        full: 45 3 * * 0
        incremental: 45 3 * * 1-6
      retentionFull:
        type: count
        value: "12"

    postgresVersion: 14
    imagePgBackRest: null
    imagePostgres: null
    patroni:
      dynamicConfiguration:
        postgresql:
          parameters:
            log_connections: on
            log_disconnections: on
            log_duration: on
            logging_collector: off
            log_statement: all
            ${length(var.db_config) == 0 ? "" : yamlencode(var.db_config)}
    EOT
    ,
    file("${path.module}/db_resources.yaml"),
    <<-EOT
    podAnnotations:
      monitoring-org-id: "${var.monitoring_org_id}"
    EOT
  ]
}
