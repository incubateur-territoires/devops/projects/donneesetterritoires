locals {
  mc_alias_command        = "mc alias set scw \"$S3_ENDPOINT\" \"$S3_ACCESS_KEY\" \"$S3_SECRET_KEY\""
  meta_json_purge_command = "mc rm --recursive --versions --force --non-current \"scw/$S3_BUCKET/docs/assets/unversioned/\""
}
resource "kubernetes_cron_job_v1" "purge_meta_json" {
  metadata {
    name      = "purge-meta-json"
    namespace = var.namespace
  }
  spec {
    schedule = "0 2 * * *"
    job_template {
      metadata {}
      spec {
        template {
          metadata {}
          spec {
            container {
              name    = "mc"
              image   = "minio/mc:RELEASE.2024-06-12T14-34-03Z"
              command = ["/bin/sh", "-c", "set -o nounset && ${local.mc_alias_command} && ${local.meta_json_purge_command}"]
              env {
                name = "S3_ACCESS_KEY"
                value_from {
                  secret_key_ref {
                    name = kubernetes_secret_v1.grist_secret.metadata[0].name
                    key  = "s3_access_key"
                  }
                }
              }
              env {
                name = "S3_SECRET_KEY"
                value_from {
                  secret_key_ref {
                    name = kubernetes_secret_v1.grist_secret.metadata[0].name
                    key  = "s3_secret_key"
                  }
                }
              }
              env {
                name  = "S3_BUCKET"
                value = scaleway_object_bucket.grist_snapshots.name
              }
              env {
                name  = "S3_ENDPOINT"
                value = "https://${var.s3_endpoint}"
              }
            }
          }
        }
      }
    }
  }
}
