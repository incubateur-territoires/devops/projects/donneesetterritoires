variable "project_slug" {
  type = string
}
variable "override_namespace" {
  type    = string
  default = null
}

variable "redirect_from" {
  type    = string
  default = null
}
variable "domain" {
  type = string
}

variable "scaleway_project_config" {
  type = object({
    project_id = string
    access_key = string
    secret_key = string
  })
  sensitive = true
}

variable "oauth_domain" {
  type = string
}

variable "oauth_client_id" {
  type = string
}

variable "oauth_client_secret" {
  type      = string
  sensitive = true
}
variable "oauth_scopes" {
  type    = string
  default = "openid email profile organization"
}
variable "oidc_protections" {
  type    = string
  default = null
}
variable "oidc_acr_values" {
  type    = string
  default = null
}
variable "oidc_extra_metadata" {
  type    = string
  default = null
}
variable "oidc_ignore_email_verified" {
  type    = string
  default = null
}

variable "default_email" {
  type = string
}

variable "monitoring_org_id" {
  type      = string
  sensitive = true
}

variable "grist_doc_wk_replicas" {
  type = number
}
variable "grist_doc_wk_requests_cpu_m" {
  type = number
}
variable "grist_doc_wk_limits_memory_mb" {
  type = number
}
variable "grist_home_wk_replicas" {
  type = number
}
variable "grist_home_wk_requests_cpu_m" {
  type = number
}
variable "grist_home_wk_limits_memory_mb" {
  type = number
}
variable "gvisor_limit_memory_bytes" {
  type = number
}
locals {
  grist_home_wk_container_limits_memory = "${var.grist_home_wk_limits_memory_mb}Mi"
  grist_doc_wk_container_limits_memory  = "${var.grist_doc_wk_limits_memory_mb}Mi"
  grist_home_wk_container_requests_cpu  = "${var.grist_home_wk_requests_cpu_m}m"
  grist_doc_wk_container_requests_cpu   = "${var.grist_doc_wk_requests_cpu_m}m"
}

variable "grist_extra_env" {
  type    = map(string)
  default = {}
}
variable "cors_allow_origin" {
  type = string
}

variable "image_repository" {
  type = string
}
variable "image_tag" {
  type = string
}

variable "database_volume_size" {
  type    = string
  default = "10Gi"
}

variable "custom_logo" {
  type = object({
    path    = string
    content = string
  })
  default = null
}

variable "custom_css" {
  type = object({
    path    = string
    content = string
  })
  default = null
}

variable "custom_script" {
  type = object({
    path    = string
    content = string
  })
  default = null
}

variable "s3_endpoint" {
  type    = string
  default = "s3.fr-par.scw.cloud"
}

variable "db_config" {
  type    = map(string)
  default = {}
}

variable "with_security_txt" {
  default = 1
}
