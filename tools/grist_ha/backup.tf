resource "scaleway_object_bucket" "grist_snapshots_backup" {
  name = "${var.project_slug}-snapshots-backup"

  lifecycle_rule {
    enabled = true
    expiration {
      days = 31
    }
    transition {
      days          = 3
      storage_class = "GLACIER"
    }
  }
}
resource "kubernetes_cron_job_v1" "snapshots_backup" {
  metadata {
    namespace = var.namespace
    name      = "snapshots-backup"
  }
  spec {
    schedule = "0 0 * * *"
    job_template {
      metadata {}
      spec {
        template {
          metadata {}
          spec {
            container {
              name  = "snapshots-backup"
              image = "registry.gitlab.com/incubateur-territoires/devops/grist3:latest"
              env {
                name  = "S3_ACCESS_KEY"
                value = var.scaleway_project_config.access_key
              }
              env {
                name  = "S3_SECRET_KEY"
                value = var.scaleway_project_config.secret_key
              }
              env {
                name  = "S3_ENDPOINT_URL"
                value = "https://s3.fr-par.scw.cloud"
              }
              env {
                name  = "S3_SRC_BUCKET"
                value = scaleway_object_bucket.grist_snapshots.name
              }
              env {
                name  = "S3_DST_BUCKET"
                value = scaleway_object_bucket.grist_snapshots_backup.name
              }
              env {
                name  = "S3_DST_PATH"
                value = "backup"
              }
              env {
                name  = "S3_STORAGE_CLASS"
                value = "STANDARD"
              }
              resources {
                requests = {
                  "memory" = "512Mi"
                }
                limits = {
                  "memory" = "512Mi"
                }
              }
            }
          }
        }
      }
    }
  }
}
